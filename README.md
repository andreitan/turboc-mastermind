![MASTERMIND](images/title.png)
## ABOUT 

This is a game derived from the traditional Mastermind, made with C and designed solely for [Turbo C++ 3.0](https://github.com/vineetchoudhary/turbocpp).

## FAQs 

### The title isn't displaying properly. 

The source code may have been opened in the wrong encoding format. See [this article](https://support.office.com/en-us/article/Choose-text-encoding-when-you-open-and-save-files-60d59c21-88b5-4006-831c-d536d42fd861) for more information, and ensure that the source code has been opened in the MS-DOS format. 

## SCREENSHOTS 

![Screenshot 1](images/screenshot1.png) 
![Screenshot 2](images/screenshot2.png) 
![Screenshot 3](images/screenshot3.png) 