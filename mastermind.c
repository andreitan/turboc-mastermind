#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<time.h> // For game RNG
#include<string.h> // For strlen
#include<graphics.h>
#include<dos.h>

FILE *scorefile;

// https://en.wikipedia.org/wiki/Box-drawing_character
// https://en.wikipedia.org/wiki/Code_page_437

void intro(), game(int ndigit), borders(int diff); // Intro, main game and border drawing
void menu(), chdiff(), scores(), help(); // Game menus
void drawbg(int color), drawtitle(int color); // Draw menu's background and title
// I need faster rendering techniques, but not now.

// Text mode is 80 chars wide, 25 tall.
// Graphics mode is 640x480 pixels.
// EASY: 5040
// MEDIUM: 30240
// HARD: 1000000

const char maker[] = "MADE BY ANDREI TAN", bgi[] = "C:\\TURBOC3\\BGI"; // For easy config
const char fwrong[] = "Score file cannot be read and must be fixed or erased.";
const char *mmind[10] = { // Array of strings for drawing Mastermind
	"�������ͻ            �ͻ                                  �ͻ",
	"� ɻ ɻ �            � �                                  � �",
	"� �� �� �����ͻ����ͻ� �ͻ����ͻ���ͻ�������ͻ�ͻ����ͻ��ͼ �",
	"� �� �� ���ͻ �� ��ͼ� �ͼ� ɻ �� �ͼ� ɻ ɻ �� �� ɻ �� ɻ �",
	"� �� �� ���ͼ �� ��ͻ� �  � ȼ �� �  � �� �� �� �� �� �� �� �",
	"� �� �� �� ɻ ���ͻ �� �  � ��ͼ� �  � �� �� �� �� �� �� �� �",
	"� �� �� �� ȼ ���ͼ �� �ͻ� ��ͻ� �  � �� �� �� �� �� �� ȼ �",
	"� ��ͼ�ͼ����ͼ����ͼ���ͼ����ͼ�ͼ  �ͼ�ͼ�ͼ�ͼ�ͼ�ͼ����ͼ",
	"� ���������������������������������������������������������ͻ",
	"�ͼ��������������������������������������������������������ͼ"
};
const char *mmindi[2] = { // Array of strings for drawing the I dot
	"                                              �ͻ            ",
	"                                              �ͼ            "
};
const char *menucho[10] = { // List of menu choices
	"NEW GAME",
	"[NEW GAME]",
	"RECENT SCORES",
	"[RECENT SCORES]",
	"HELP",
	"[HELP]",
	"QUIT",
	"[QUIT]"
};
const char *diffcho[8] = { // List of difficulty choices
	"EASY - 4",
	"[EASY - 4]",
	"MEDIUM - 5",
	"[MEDIUM - 5]",
	"HARD - 6,rep",
	"[HARD - 6,rep]",
	"BACK",
	"[BACK]"
};
const char *scorescho[8] = { // List of scores choices
	"ERASE",
	"[ERASE]",
	"BACK",
	"[BACK]"
};
const char *helptext[11] = { // Self-explanatory, no spaces after commas due to text rendering
	"In Mastermind,you must try to guess the",
	"correct sequence in as few attempts as possible.",
	"",
	"Each time you input a guess,the game responds by showing:",
	"- how many digits are both part of the",
	"sequence and in the correct position;",
	"- how many digits are part of the",
	"sequence, but not in the correct position; and",
	"- how many digits are not part of the sequence at all.",
	"",
	"You can enter \"forfeit\" to return to the menu any time."
};
const char *instr[6] = { // Menu navigation instructions
	"[W] and [S] to navigate",
	"[ENTER] to select"
};

int main() {
	scorefile = fopen("mms.dta", "r+t"); // Read and write on file if it exists
	if (scorefile == NULL) scorefile = fopen("mms.dta", "w+t"); // Create file if not
	intro();
	menu();
	nosound();
	fclose(scorefile);
	return(0);
}

void intro() {
	int gd = DETECT, gm, i;
	initgraph(&gd, &gm, bgi);
	cleardevice();
	settextstyle(SANS_SERIF_FONT, HORIZ_DIR, 2);
	setcolor(RED);
	circle(getmaxx()/2 - 35, getmaxy()/2 - 40, 10);
	setcolor(WHITE);
	setfillstyle(SOLID_FILL, WHITE);
	fillellipse(getmaxx()/2 + 35, getmaxy()/2 - 40, 15, 15);
	setfillstyle(EMPTY_FILL, BLACK);
	delay(150);
	for (i = 0; i < 30; i++) {
		arc(getmaxx()/2, getmaxy()/2 - 55, 210, 210 + (4 * i), 100);
		delay(5);
	}
	outtextxy(getmaxx()/2 - textwidth(maker)/2, getmaxy()/2 - textheight(maker)/2 + 70, maker);
	sound(1046.5);
	delay(200);
	nosound();
	delay(500);
	cleardevice();
	closegraph();
}

void menu() { // Main menu
	int gd = DETECT, gm, choice = 0, i, y;
	char c;
	initgraph(&gd, &gm, bgi);
	draw:
	if (choice == 0) {
		drawbg(LIGHTCYAN);
		drawtitle(LIGHTCYAN);
	}
	else if (choice == 1) {
		drawbg(YELLOW);
		drawtitle(YELLOW);
	}
	else if (choice == 2) {
		drawbg(LIGHTGREEN);
		drawtitle(LIGHTGREEN);
	}
	else {
		drawbg(LIGHTRED);
		drawtitle(LIGHTRED);
	}
	settextstyle(3, HORIZ_DIR, 2);
	for (i = 0, y = 240; i < 8; i += 2) {
		outtextxy(getmaxx()/2 - textwidth(menucho[i == choice * 2 ? i + 1 : i])/2, y,
			menucho[i == choice * 2 ? i + 1 : i]);
		y += textheight(menucho[i]);
	}
	choose:
	c = getch();
	if ((c == 'W' || c == 'w') && choice > 0) {
		choice--;
		goto draw;
	} else if ((c == 'S' || c == 's') && choice < 3) {
		choice++;
		goto draw;
	} else if (c == 13) {
		nosound();
		cleardevice();
		closegraph();
		if (choice == 0) chdiff();
		else if (choice == 1) scores();
		else if (choice == 2) help();
		else return;
		initgraph(&gd, &gm, bgi);
		goto draw;
	} else goto choose;
}

void chdiff() { // Difficulty menu
	int gd = DETECT, gm, choice = 0, i, y;
	char c;
	initgraph(&gd, &gm, bgi);
	draw:
	if (choice == 0) {
		drawbg(LIGHTBLUE);
		drawtitle(LIGHTBLUE);
	}
	else if (choice == 1) {
		drawbg(LIGHTGREEN);
		drawtitle(LIGHTGREEN);
	}
	else if (choice == 2) {
		drawbg(LIGHTRED);
		drawtitle(LIGHTRED);
	}
	else {
		drawbg(YELLOW);
		drawtitle(YELLOW);
	}
	settextstyle(3, HORIZ_DIR, 2);
	for (i = 0, y = 240; i < 6; i += 2) {
		outtextxy(getmaxx()/2 - textwidth(diffcho[i == choice * 2 ? i + 1 : i])/2, y,
			diffcho[i == choice * 2 ? i + 1 : i]);
		y += textheight(diffcho[i]);
	}
	y += textheight(diffcho[i]);
	outtextxy(getmaxx()/2 - textwidth(diffcho[i == choice * 2 ? i + 1 : i])/2, y,
			diffcho[i == choice * 2 ? i + 1 : i]);
	choose:
	c = getch();
	if ((c == 'W' || c == 'w') && choice > 0) {
		choice--;
		goto draw;
	} else if ((c == 'S' || c == 's') && choice < 3) {
		choice++;
		goto draw;
	} else if (c == 13) {
		nosound();
		cleardevice();
		closegraph();
		if (choice <= 2) game(choice + 4);
		return;
	} else goto choose;
}

void scores() { // Show 10 most recent scores and respective difficulties
	int gd = DETECT, gm, choice = 1, i, y, m, n;
	char c, score[40], diff, tries[32];
	initgraph(&gd, &gm, bgi);
	draw:
	drawbg(choice >= 1 ? YELLOW : LIGHTRED);
	settextstyle(3, HORIZ_DIR, 1);
	rewind(scorefile);
	n = 0;
	while (fgets(score, 40, scorefile) != NULL) { // Read each line from mms
		m = sscanf(score, "%c %s", &diff, &tries);
		n++;
		if (m != 2 || n > 10 || diff < '4' || diff > '6') { // Format break detected
			setcolor(WHITE);
			outtextxy(getmaxx()/2 - textwidth(fwrong)/2, 240, fwrong);
			goto draw2;
		}
		for (i = 0; tries[i] != '\0'; i++) if (tries[i] < '0' || tries[i] > '9') {
			setcolor(WHITE);
			outtextxy(getmaxx()/2 - textwidth(fwrong)/2, 240, fwrong);
			goto draw2;
		}
	}
	rewind(scorefile);
	y = 60;
	while (fgets(score, 40, scorefile) != NULL) {
		sscanf(score, "%c %s", &diff, &tries);
		if (diff == '4') setcolor(LIGHTBLUE);
		else if (diff == '5') setcolor(LIGHTGREEN);
		else setcolor(LIGHTRED);
		outtextxy(getmaxx()/2 - textwidth(score)/2, y, score);
		y += textheight(score);
	}
	draw2:
	setcolor(WHITE);
	settextstyle(3, HORIZ_DIR, 2);
	for (i = 0, y = 360; i < 4; i += 2) {
		outtextxy(getmaxx()/2 - textwidth(scorescho[i == choice * 2 ? i + 1 : i])/2, y,
			scorescho[i == choice * 2 ? i + 1 : i]);
		y += textheight(scorescho[i]);
	}
	choose:
	c = getch();
	if ((c == 'W' || c == 'w') && choice > 0) {
		choice--;
		goto draw;
	} else if ((c == 'S' || c == 's') && choice < 1) {
		choice++;
		goto draw;
	} else if (c == 13) {
		if (choice == 0) {
			fclose(scorefile);
			scorefile = fopen("mms.dta", "w+t"); // Replace with blank file
			goto draw;
		} else {
		nosound();
			cleardevice();
			closegraph();
			return;
		}
	} else goto choose;
}

void help() { // Instructions
	int gd = DETECT, gm, i, y;
	char c;
	initgraph(&gd, &gm, bgi);
	drawbg(LIGHTGREEN);
	settextstyle(3, HORIZ_DIR, 1);
	for (i = 0, y = 60; i < 11; i++) {
		outtextxy(getmaxx()/2 - textwidth(helptext[i])/2, y, helptext[i]);
		y += textheight(helptext[i]);
	}
	outtextxy(getmaxx()/2 - textwidth("[BACK]")/2, 360, "[BACK]");
	choose:
	c = getch();
	if (c == 13) {
		nosound();
		cleardevice();
		closegraph();
		return;
	} else goto choose;
}

void game(int ndigit) { // Mastermind itself
	char answer[7], input[8], vallist[7], c, score[40], diff, stries[40], *ep;
	// answer and input are self-explanatory
	// vallist is a helper array for checking hits
	// c is for helping input and y/n getch
	// diff, stries, and ep are for scorewriting
	int i, j, correct, almost, wrong, inputend, tempdiffs[10], m, n;
	// i and j are for iterations
	// correct, almost, and wrong are the number of respective hits
	// inputend is a boolean for input
	// tempdiffs, m, and n are for scorewriting
	unsigned long tries, temptries[10];
	// tries is how many times you fail
	// temptries is for scorewriting
	start:
	tries = 0;
	randomize();
	for (i = 0; i < ndigit; i++) { // Generate sequence
		seed:
		answer[i] = random(10) + '0';
		if (ndigit < 6) for (j = 0; j < i; j++) if (answer[i] == answer[j]) goto seed;
	}
	answer[ndigit] = '\0';
	if (ndigit >= 6) textcolor(LIGHTRED);
	else if (ndigit <= 4) textcolor(LIGHTBLUE);
	else textcolor(LIGHTGREEN);
	_setcursortype(_NORMALCURSOR);
	clrscr();
	// Draw top part
	gotoxy(1, 1);
	cprintf("�");
	for (i = 2; i < 80; i++) cprintf("�");
	cprintf("�");
	borders(ndigit);
	prepare:
	tries++;
	correct = 0;
	almost = 0;
	wrong = 0;
	nosound();
	borders(ndigit);
	gotoxy(3, wherey() - 1);
	input:
	for (i = 7; i >= 0; i--) input[i] = '\0';
	i = 0;
	inputend = 0;
	// http://stackoverflow.com/questions/26254297/
	do { // Custom input
		gotoxy(3, wherey());
		cprintf("                     "); // Wipe clean.
		gotoxy(3, wherey());
		if (ndigit >= 6) textcolor(LIGHTRED);
		else if (ndigit <= 4) textcolor(LIGHTBLUE);
		else textcolor(LIGHTGREEN);
		cprintf("%lu: ", tries);
		textcolor(WHITE);
		cprintf("%s", input);
		c = getch();
		switch (c) {
			case 8: // Backspace
				if (i > 0) {
					i--;
					input[i] = 0;
				}
				break;
			case 13: // Enter
				if (i > 0) inputend = 1;
				break;
			default:
				if (i < 7 && ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z')
					|| (c >= 'A' && c <= 'Z'))) {
					input[i] = c;
					i++;
					input[i] = 0;
				}
		}
	} while (inputend < 1);
	if (strcmpi(input, "forfeit") == 0) return;
	else if (strcmpi(input, "devdoor") == 0) { // Secret password
		gotoxy(3, wherey());
		cprintf("\nDEV DOOR ACTIVATED: %s", answer);
		borders(ndigit);
		// Draw bottom part
		_setcursortype(_NOCURSOR);
		borders(ndigit);
		if (ndigit >= 6) textcolor(LIGHTRED);
		else if (ndigit <= 4) textcolor(LIGHTBLUE);
		else textcolor(LIGHTGREEN);
		cprintf("�");
		for (i = 2; i < 80; i++) cprintf("�");
		cprintf("�");
		getch();
		return;
	} else {
		strcpy(vallist, answer);
		if (strlen(input) != ndigit) goto input;
		for (i = 0; input[i] != '\0'; i++) if (input[i] < '0' || input[i] > '9') goto input;
		for (i = 0; input[i] != '\0'; i++) if (input[i] == vallist[i]) {
			correct++;
			input[i] = 'C'; // Replacing input[i] and vallist[i] prevents double-assignment
			vallist[i] = 'D';
		}
		for (i = 0; input[i] != '\0'; i++) {
			for (j = 0; input[j] != '\0'; j++) if (input[i] == vallist[j]) {
				almost++;
				vallist[j] = 'D';
				break;
			}
		}
		wrong = ndigit - correct - almost; // Hah, lazy.
		cprintf("\n");
		if (correct >= ndigit) { // SUCCESS!
			rewind(scorefile);
			n = 0;
			while (fgets(score, 40, scorefile) != NULL) { // Read each line from mms
				m = sscanf(score, "%i %s", &tempdiffs[n], &stries);
				if (m != 2 || tempdiffs[n] < 4 || tempdiffs[n] > 6) goto win;
				for (i = 0; stries[i] != '\0'; i++) // Write nothing if file is tampered
					if (stries[i] < '0' || stries[i] > '9') goto win;
				temptries[n] = strtoul(stries, &ep, 10);
				n++;
				if (n > 9) { // Only 10 recent scores will be recorded
					for (i = 0; i < 9; i++) {
						tempdiffs[i] = tempdiffs[i + 1];
						temptries[i] = temptries[i + 1];
					}
					fclose(scorefile);
					scorefile = fopen("mms.dta", "w+t"); // Replace with blank file
					for (i = 0; i < 9; i++) 
						fprintf(scorefile, "%i %lu\n", tempdiffs[i], temptries[i]);
				}
			}
			fprintf(scorefile, "%i %lu\n", ndigit, tries); // Write score data to mms
			win:
			_setcursortype(_NOCURSOR);
			sound(1046.5);
			delay(200);
			nosound();
			gotoxy(3, wherey());
			cprintf("Congratulations! You correctly deduced the sequence in %lu tr", tries);
			cprintf(tries == 1 ? "y." : "ies.");
			borders(ndigit);
			gotoxy(3, wherey());
			cprintf("Would you like to play again? [Y/N]");
			borders(ndigit);
			// Draw bottom part
			borders(ndigit);
			if (ndigit >= 6) textcolor(LIGHTRED);
			else if (ndigit <= 4) textcolor(LIGHTBLUE);
			else textcolor(LIGHTGREEN);
			cprintf("�");
			for (i = 2; i < 80; i++) cprintf("�");
			cprintf("�");
			choice:
			c = getch();
			if (c == 'Y' || c == 'y') goto start;
			else if (c == 'N' || c == 'n') return;
			else goto choice;
		} else { // Try again
			gotoxy(3, wherey());
			cprintf("%i %i %i", correct, almost, wrong);
			borders(ndigit);
			goto prepare;
		}
	}
}

void borders(int diff) { // Draw game borders
	if (diff >= 6) textcolor(LIGHTRED);
	else if (diff <= 4) textcolor(LIGHTBLUE);
	else textcolor(LIGHTGREEN);
	gotoxy(1, wherey());
	cprintf("�");
	gotoxy(80, wherey());
	cprintf("�");
	textcolor(WHITE);
}

void drawbg(int color) { // Draw menu background
	int i, y;
	int squang[] = {560, 440, 600, 400, 560, 360, 520, 400, 560, 440};
	// squang is for drawing the "diamond"
	setcolor(color);
	cleardevice();
	line(640, 240, 400, 480);
	rectangle(500, 360, 540, 400);
	drawpoly(5, squang);
	circle(120, 300, 60);
	settextstyle(3, HORIZ_DIR, 2);
	setcolor(WHITE);
	for (i = 0, y = 400; i < 2; i++) {
		outtextxy(40, y, instr[i]);
		y += textheight(instr[i]);
	}
}

void drawtitle(int color) { // Draw fabulous title
	int i, y;
	settextstyle(0, HORIZ_DIR, 1);
	setcolor(color);
	for (i = 0, y = 100; i < 2; i++) {
		outtextxy(getmaxx()/2 - textwidth(mmindi[i])/2, y, mmindi[i]);
		y += textheight(mmindi[i]);
	}
	setcolor(WHITE);
	for (i = 0, y = 100; i < 10; i++) {
		outtextxy(getmaxx()/2 - textwidth(mmind[i])/2, y, mmind[i]);
		y += textheight(mmind[i]);
	}
}